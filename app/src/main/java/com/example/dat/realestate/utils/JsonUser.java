package com.example.dat.realestate.utils;

/**
 * Created by hiennq.
 */
public interface JsonUser {
    public static final String TOKEN="e3MKLARqvarb0w5-t_UvQk1wf4tCiU93";

    public static final String URL_SERVICE="http://54.169.254.24:81";

    public static final String URL_REGISTER=URL_SERVICE+"/backend/post/register?token="+TOKEN+"&";

    public static final String URL_LOGIN=URL_SERVICE+"/backend/post/login?token="+TOKEN+"&";

    public static final String URL_GET_USER_BY_EMAIL=URL_SERVICE+"/backend/getjson/get_user_by_email?token="+TOKEN+"&email=";

    public static final String URL_GET_USER=URL_SERVICE+"/backend/getjson/users?token="+TOKEN+"&";

    public static final String URL_GET_USER_BY_ID=URL_SERVICE+"/backend/getjson/users?token="+TOKEN+"&id=";

    public static final String URL_GET_USER_BY_USERNAME=URL_SERVICE+"/backend/getjson/users?token="+TOKEN+"&username=";

}
