package com.example.dat.realestate.models;


import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;

/**
 * Created by Dat on 4/30/2016.
 */

public class Rental extends BaseModel {
    private int id;
    private int user_id;
    private double longitude;
    private double latitude;
    private double price;
    private String photo1;
    private String photo2;
    private String location;
    private double area;
    private String description;
    private long created_at;
    private long updated_at;
    private String type;
    private JSONObject passToCustomWindow;
    private int rate_point;
    private int rate_count;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */


    public void addMarker(GoogleMap googleMap){
        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(this.latitude, this.longitude))
                .snippet(passToCustomWindow.toString())
                .title(this.location));
    }

    public void setDataFromJsonObject(){
        try {
            this.id = jsonData.getInt("id");
            this.user_id = jsonData.getInt("user_id");
            this.longitude = jsonData.getDouble("longitude");
            this.latitude = jsonData.getDouble("latitude");
            this.price = jsonData.getDouble("price");
            this.location = jsonData.getString("location");
            this.area = jsonData.getDouble("area");
            this.description = jsonData.getString("description");
            this.photo1 = jsonData.getString("photo1");
            this.photo2 = jsonData.getString("photo2");
            this.rate_point = jsonData.getInt("rate_point");
            this.rate_count = jsonData.getInt("rate_count");
            this.type = jsonData.getString("type");
            this.created_at = jsonData.getLong("created_at");
            this.updated_at = jsonData.getLong("updated_at");
            passToCustomWindow = jsonData;

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // default constructor

    public Rental() {
        super();
    }

    public void setHashData(){
        hashData.put("id", Integer.toString(this.id));
        hashData.put("user_id",Integer.toString(this.user_id));
        hashData.put("longitude",Double.toString(this.longitude));
        hashData.put("latitude",Double.toString(this.latitude));
        hashData.put("price",Double.toString(this.price));
        hashData.put("location",this.location);
        hashData.put("area",Double.toString(this.area));
        hashData.put("description",this.description);
        hashData.put("rate_point",Integer.toString(this.rate_point));
        hashData.put("rate_count",Integer.toString(this.rate_count));
        hashData.put("photo1",this.photo1);
        hashData.put("photo2",this.photo2);
        hashData.put("created_at",Long.toString(this.created_at));
        hashData.put("updated_at",Long.toString(this.updated_at));
    }

    public float calRatePoint(){
        return rate_point/rate_count;
    }

    // getter and setter

    public JSONObject getPassToCustomWindow() {
        return passToCustomWindow;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public double getArea() {
        return area;
    }

    public void setArea(float area) {
        this.area = area;
    }

    public String getPhoto1(){
        return photo1;
    }

    public void setPhoto1(String photo1){
        this.photo1 = photo1;
    }

    public String getPhoto2(){ return photo2; }

    public void setPhoto2(String photo1){ this.photo2 = photo2; }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType(){ return this.type;}

    public void setType(String type){ this.type = type;}

    public long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

    public long getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(long updated_at) {
        this.updated_at = updated_at;
    }

    public double getRate(){
        return rate_point/rate_count;
    }

    public int getRateCount(){
        return this.rate_count;
    }

    public static String parseDateString(long unixtimestamp){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String date = sdf.format(unixtimestamp*1000);
        return date;
    }
}
