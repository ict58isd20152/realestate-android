package com.example.dat.realestate.utils;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.dat.realestate.MapsActivity;
import com.example.dat.realestate.R;
import com.example.dat.realestate.models.Rental;
import com.example.dat.realestate.viewDetail;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by hiennq on 5/16/16.
 */
public class RVAdapter extends RecyclerView.Adapter<RVAdapter.PersonViewHolder> implements JsonRental{
    List<Rental> persons;
    private static Context context;

    public RVAdapter(Context aContext,List<Rental> persons){
        this.persons = persons;
        this.context = aContext;
    }

    @Override
    public int getItemCount() {
        return persons.size();
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_layout, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PersonViewHolder personViewHolder, int i) {
        personViewHolder.currentItem = persons.get(i);
        personViewHolder.type.setText("Type: " + persons.get(i).getType());
        personViewHolder.address.setText("Address: " + persons.get(i).getLocation());
        personViewHolder.price.setText("Price: " + persons.get(i).getPrice() +" $");
        personViewHolder.created_at.setText("Date: " + persons.get(i).parseDateString(persons.get(i).getCreated_at()));
        personViewHolder.ratingBar.setRating((float) persons.get(i).getRate());
        personViewHolder.rate_count.setText("Rate: " + persons.get(i).getRateCount() + " times");
        Picasso.with(context)
                .load(persons.get(i).getPhoto1())
                .centerInside()
                .resize(128,128)
                        //.placeholder(R.mipmap.ic_launcher)
                .into(personViewHolder.image);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class PersonViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        ImageView image;
        TextView type;
        TextView address;
        TextView price;
        TextView created_at;
        TextView rate_count;
        RatingBar ratingBar;
        public View view;
        public Rental currentItem;
        Intent intent;

        PersonViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            cv = (CardView)itemView.findViewById(R.id.cv);
            image = (ImageView) itemView.findViewById(R.id.image);
            type = (TextView) itemView.findViewById(R.id.type);
            address = (TextView) itemView.findViewById(R.id.address);
            price = (TextView) itemView.findViewById(R.id.price);
            ratingBar = (RatingBar) itemView.findViewById(R.id.ratingBar);
            created_at = (TextView) itemView.findViewById(R.id.created_at);
            rate_count = (TextView) itemView.findViewById(R.id.rate_count);
            view.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    intent = new Intent(context, viewDetail.class);
                    intent.putExtra("jsonObject", currentItem.getJsonData().toString());
                    context.startActivity(intent);
                }
            });
        }
    }

}