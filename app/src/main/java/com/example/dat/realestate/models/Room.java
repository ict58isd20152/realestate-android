package com.example.dat.realestate.models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Dat on 5/1/2016.
 */
public class Room extends Rental {
    public Room(){
        super();
        setTableName("Rooms");
    }
    private boolean rest_room;

    @Override
    public void setDataFromJsonObject (){
        super.setDataFromJsonObject();
        try {
            this.rest_room = jsonData.getBoolean("rest_room");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setHashData(){
        super.setHashData();
        hashData.put("rest_room", Boolean.toString(this.rest_room));
    }

    public void setRest_room(boolean rest_room){this.rest_room = rest_room;}
    public boolean getRest_room(boolean rest_room){return this.rest_room;}
}
