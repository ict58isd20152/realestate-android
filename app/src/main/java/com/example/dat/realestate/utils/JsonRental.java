package com.example.dat.realestate.utils;

/**
 * Created by hiennq.
 */
public interface JsonRental {
    public static final String TOKEN="e3MKLARqvarb0w5-t_UvQk1wf4tCiU93";

    public static final String URL_SERVICE="http://54.169.254.24:81";

    public static final String URL_GET_ROOM=URL_SERVICE+"/backend/getjson/rooms?token="+TOKEN+"&";

    public static final String URL_GET_ROOM_BY_ID=URL_SERVICE+"/backend/getjson/rooms?token="+TOKEN+"&id=";

    public static final String URL_GET_HOUSE=URL_SERVICE+"/backend/getjson/houses?token="+TOKEN+"&";

    public static final String URL_GET_HOUSE_BY_ID=URL_SERVICE+"/backend/getjson/houses?token="+TOKEN+"&id=";

    public static final String URL_GET_APARTMENT=URL_SERVICE+"/backend/getjson/apartments?token="+TOKEN+"&";

    public static final String URL_GET_APARTMENT_BY_ID=URL_SERVICE+"/backend/getjson/apartments?token="+TOKEN+"&id=";

    public static final String URL_GET_PROPERTY=URL_SERVICE+"/backend/getjson/property?token="+TOKEN+"&";

    public static final String URL_GET_LIST=URL_SERVICE+"/backend/getjson/list_property?token="+TOKEN+"&";

    public static final String URL_POST_PROPERTY=URL_SERVICE+"/backend/post/rental?token="+TOKEN+"&";
}
