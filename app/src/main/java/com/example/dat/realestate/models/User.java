package com.example.dat.realestate.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Dat on 4/30/2016.
 */
public class User extends BaseModel{
    private int id;
    private int plan_id;
    private byte role;
    private boolean status;
    private String username;
    private String avatar;
    private String full_name;
    private String phone_number;
    private long created_at;
    private long updated_at;
    private JSONObject UserObj;
    private String address;
    private String email;

    public void setDataFromJsonObject() {
        try {
            this.id = jsonData.getInt("id");
            this.plan_id = jsonData.getInt("plan_id");
            this.role = (byte) jsonData.getInt("role");
            this.status = jsonData.getBoolean("status");
            this.username = jsonData.getString("username");
            this.email = jsonData.getString("email");
            this.avatar = jsonData.getString("avatar");
            this.phone_number = jsonData.getString("phone_number");
            this.full_name = jsonData.getString("full_name");
            this.address = jsonData.getString("address");
            this.email = jsonData.getString("email");
            this.created_at = jsonData.getLong("created_at");
            this.updated_at = jsonData.getLong("updated_at");
            UserObj = jsonData;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setHashData(){
        hashData.put("id", Integer.toString(this.id));
        hashData.put("plan_id",Integer.toString(this.plan_id));
        hashData.put("role",Byte.toString(this.role));
        hashData.put("status",Boolean.toString(this.status));
        hashData.put("username",this.username);
        hashData.put("avatar",this.avatar);
        hashData.put("full_name",this.full_name);
        hashData.put("phone_number",this.phone_number);
        hashData.put("address",this.address);
        hashData.put("email",this.email);
        hashData.put("created_at",Long.toString(this.created_at));
        hashData.put("updated_at",Long.toString(this.updated_at));
    }

    // default constructor

    public User() {
        super();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(int plan_id) {
        this.plan_id = plan_id;
    }

    public byte getRole() {
        return role;
    }

    public void setRole(byte role) {
        this.role = role;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public void setEmail(String email){ this.email = email; }

    public String getEmail(){return this.email;}

    public void setAddress(String email){ this.address = address; }

    public String getAddress(){return this.address;}

    public long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

    public long getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(long updated_at) {
        this.updated_at = updated_at;
    }

    public JSONObject getUserObj() {
        return UserObj;
    }
}
