package com.example.dat.realestate.utils;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dat.realestate.MapsActivity;
import com.example.dat.realestate.R;
import com.example.dat.realestate.viewDetail;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Dat on 5/1/2016.
 */

public class customInfoWindow implements InfoWindowAdapter {
    private View popup=null;
    private LayoutInflater inflater=null;
    ImageView imgv;
    TextView tv;
    JSONObject content;
    Intent intent;


    public customInfoWindow(LayoutInflater inflater) {
        this.inflater=inflater;
    }
    @Override
    public View getInfoWindow(Marker marker) {
        return(null);
    }

    @Override
    public View getInfoContents(Marker marker) {
        if (popup == null) {
            popup=inflater.inflate(R.layout.custominfowindow, null);
        }

        imgv = (ImageView)popup.findViewById(R.id.icon);
        String image = null;

        tv=(TextView)popup.findViewById(R.id.title);
        tv.setText(marker.getTitle());

        tv=(TextView)popup.findViewById(R.id.snippet);
        try {
            do {} while (marker.getSnippet() == null );
            content = new JSONObject(marker.getSnippet());
            tv.setText("Price: " + content.getString("price") + " " + "\n"
                    + "Area: " + content.getString("area") + " m2");
//            tv.setText(content.getString("photo1"));
            image = content.getString("photo1");
        } catch (JSONException e) {}
        Picasso.with(MapsActivity.getContext())
                .load(image)
                //.centerInside()
                .resize(350,350)
                .placeholder(R.mipmap.ic_launcher)
                .into(imgv, new MarkerCallback(marker));
        intent = new Intent(MapsActivity.getContext(), viewDetail.class);
        intent.putExtra("jsonObject", content.toString());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        MapsActivity.mMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                MapsActivity.getContext().startActivity(intent);
            }
        });

        return(popup);
    }
}
