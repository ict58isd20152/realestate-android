package com.example.dat.realestate.models;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;

public abstract class BaseModel implements ModelInterface{
    protected String tableName;
    protected HashMap<String,String> hashData;
    protected JSONObject jsonData;

    public BaseModel(){
        hashData = new HashMap<>();
    }

    public String getTableName(){
        return this.tableName;
    }

    public void setTableName(String tableName){
        this.tableName = tableName;
    }

    public JSONObject getJsonData(){
        return jsonData;
    }

    public abstract void setDataFromJsonObject();

    public void setJsonData(JSONObject data){
        this.jsonData=data;
        setDataFromJsonObject();
        setHashData();
    }

    public HashMap<String,String> getHashData(){
        return hashData;
    }

    public void setHashData(HashMap<String,String> data){
        this.hashData = data;
        try {
            this.jsonData = new JSONObject(new Gson().toJson(this.hashData));
        }
        catch (Exception e){
        }

    }

    public abstract void setHashData();

}
