package com.example.dat.realestate.models;

interface ModelInterface {


    public String getTableName();

    public void setTableName(String tableName);

}
