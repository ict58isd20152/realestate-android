package com.example.dat.realestate;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.support.v7.app.ActionBarDrawerToggle;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Dat on 4/30/2016.
 */

public class navMenuActivity extends AppCompatActivity {
    private ListView mDrawerList;
    private ArrayAdapter<String> mAdapter;
    private ActionBarDrawerToggle mDrawerToggle;
    protected DrawerLayout mDrawerLayout;
    private String mActivityTitle;
    Intent intent;
    ProgressDialog PD;
    String[] osArray = {"Login", "List Items", "Map"};
    private static final String[] EMPTY_STRING_ARRAY = new String[0];

    SharedPreferences prefs;
    boolean Islogin;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nav_menu);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Islogin = prefs.getBoolean("IsLogin", false);

        if (Islogin) {
            List<String> list = new ArrayList<>();
            Collections.addAll(list, osArray);
            list.removeAll(Arrays.asList("Login"));
            list.add("Post Ads");
            list.add("Logout");
            osArray = list.toArray(EMPTY_STRING_ARRAY);

        }

        mDrawerList = (ListView)findViewById(R.id.navList);
        addDrawerItems();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        mActivityTitle = getTitle().toString();
        setupDrawer();



        PD = new ProgressDialog(this);
        PD.setMessage("Loading.....");
        PD.setCancelable(false);
    }

    // item in nav bar

    private void addDrawerItems() {
        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(mAdapter);

        // handle menu click

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //System.out.println(prefs.getString("email",""));
                Islogin = prefs.getBoolean("IsLogin", false);
                if (Islogin) {
                    switch ((int) id) {
                        case 0: {
                            intent = new Intent(navMenuActivity.this, ListActivity.class);
                            startActivity(intent);
                            break;
                        }
                        case 1: {
                            intent = new Intent(navMenuActivity.this, MapsActivity.class);
                            startActivity(intent);
                            break;
                        }
                        case 2: {
                            intent = new Intent(navMenuActivity.this, CreateActivity.class);
                            startActivity(intent);
                            break;
                        }
                        case 3: {
                            prefs.edit().remove("IsLogin").apply();
                            prefs.edit().remove("email").apply();
                            finish();
                            startActivity(new Intent(getApplicationContext(), ListActivity.class));
                            break;
                        }
                        default:
                            break;
                    }
                } else {
                    switch ((int) id) {
                        case 0: {
                            if (!Islogin) {
                                intent = new Intent(navMenuActivity.this, LoginActivity.class);
                                startActivity(intent);
                                break;
                            }
                            break;
                        }
                        case 1: {
                            intent = new Intent(navMenuActivity.this, ListActivity.class);
                            startActivity(intent);
                            break;
                        }
                        case 2: {
                            intent = new Intent(navMenuActivity.this, MapsActivity.class);
                            startActivity(intent);
                            break;
                        }
                        default:
                            break;
                    }
                }
            }
        });
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("Menu");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.addDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        // Activate the navigation drawer toggle
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
