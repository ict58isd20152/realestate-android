package com.example.dat.realestate;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.dat.realestate.app.AppController;
import com.example.dat.realestate.models.User;
import com.example.dat.realestate.utils.JsonUser;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.Date;

public class viewDetail extends navMenuActivity implements JsonUser {

    TextView type,tv1,tv2,tv3,tv4,tv5;
    JSONObject jObj;
    private String urlJsonObj = URL_GET_USER_BY_ID;
    ImageView imgv1;
    ImageView imgv2;

    User user;
    int userId;
    Dialog dialog;
    TextView tv_fullname,tv_address,tv_phonenumber,tv_email;
    ImageView avatar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /// get nav menu

        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_view_detail, null, false);
        mDrawerLayout.addView(contentView, 0);

        ////
        type = (TextView) findViewById(R.id.type);
        tv1 = (TextView) findViewById(R.id.tv1);
        tv2 = (TextView) findViewById(R.id.tv2);
        tv3 = (TextView) findViewById(R.id.tv3);
        tv4 = (TextView) findViewById(R.id.tv4);
        tv5 = (TextView) findViewById(R.id.tv5);

        imgv1 = (ImageView) findViewById(R.id.imgv1);
        imgv2 = (ImageView) findViewById(R.id.imgv2);

        dialog = new Dialog(this); // Context, this, etc.
        dialog.setContentView(R.layout.dialog_demo);
        tv_fullname = (TextView) dialog.findViewById(R.id.tv_fullname);
        tv_email = (TextView) dialog.findViewById(R.id.tv_email);
        tv_address = (TextView) dialog.findViewById(R.id.address);
        tv_phonenumber = (TextView) dialog.findViewById(R.id.tv_phonenumber);
        avatar = (ImageView) dialog.findViewById(R.id.avatar);
        // receive jsonobject from previous activity
        Intent intent = getIntent();
        String jsonString = intent.getStringExtra("jsonObject");
        try {
            jObj = new JSONObject(jsonString);
            urlJsonObj = urlJsonObj + jObj.getString("user_id");
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),
                    "Error: " + e.getMessage(),
                    Toast.LENGTH_LONG).show();
        }

        // send json request for user object

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                urlJsonObj, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //User usr;
                        try {
                            type.setText(jObj.getString("type").toUpperCase());
                            String imgUrl1 = jObj.getString("photo1");
                            String imgUrl2 = jObj.getString("photo2");
                            Picasso.with(MapsActivity.getContext())
                                    .load(imgUrl1)
                                    //.centerInside()
                                    .resize(350,350)
                                    .placeholder(R.mipmap.ic_launcher)
                                    .into(imgv1);
                            Picasso.with(MapsActivity.getContext())
                                    .load(imgUrl2)
                                    //.centerInside()
                                    .resize(350,350)
                                    .placeholder(R.mipmap.ic_launcher)
                                    .into(imgv2);
                            long stamp = jObj.getLong("created_at");
                            Date date = new Date(stamp*1000);
                            DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getApplicationContext());

                            tv1.setText("Location: " + jObj.getString("location"));
                            tv2.setText("Area: " + jObj.getString("area") + " m2");
                            tv4.setText("Price: " + jObj.getString("price") + "$");
                            tv3.setText("Description: " + jObj.getString("description"));
                            tv5.setText("Created: " + dateFormat.format(date));
//                            String contact_info = "Contact: \n\n"+
//                                    response.getString("full_name") + "\nAddress: "+response.getString("address")+
//                                    "\nPhone number: "+response.getString("phone_number")+ "\nemail: "+response.getString("email")+
//                                    "\nAddress: "+response.getString("address");
                            tv_fullname.setText("Owner name: " + response.getString("full_name"));
                            tv_address.setText("Address: " + response.getString("address"));
                            tv_email.setText("Email: " + response.getString("email"));
                            tv_phonenumber.setText("Phone number: " + response.getString("phone_number"));
                            tv5.setText("Created: " + dateFormat.format(date));
                            Picasso.with(MapsActivity.getContext())
                                    .load(response.getString("avatar"))
                                    .centerInside()
                                    .resize(500,500)
                                    .placeholder(R.mipmap.ic_launcher)
                                    .into(avatar);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjReq);
        //openDialog();
    }
    public void showContact(View view){
        openDialog();
    }
    public void hideDialog(View view) {
        dialog.dismiss();
    }
    public void openDialog() {

//        dialog.setContentView(R.layout.dialog_demo);
        dialog.setTitle("Dialog");
        dialog.show();
    }
}
