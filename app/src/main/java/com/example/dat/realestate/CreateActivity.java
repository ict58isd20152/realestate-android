package com.example.dat.realestate;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import android.app.AlertDialog;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.dat.realestate.app.AppController;
import com.example.dat.realestate.models.User;
import com.example.dat.realestate.utils.JsonRental;
import com.example.dat.realestate.utils.JsonUser;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CreateActivity extends navMenuActivity implements JsonRental,JsonUser {
    EditText field_price;
    EditText field_latitude;
    EditText field_longitude;
    EditText field_location;
    EditText field_area;
    EditText field_no_room;
    EditText field_no_rest_room;
    EditText field_description;
    CheckBox checkbox_rest_room;
    TextView label_no_room;
    TextView label_no_rest_room;
    Button btn_send;
    RadioButton radio_apartment;
    RadioButton radio_room;
    RadioButton radio_house;
    int property; //1. apartment   2. room   3. houses
    private HashMap<String,String> dataView;

    private AlertDialog.Builder builder;
    private AlertDialog dialog;

    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /// get nav menu

        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_create, null, false);
        mDrawerLayout.addView(contentView, 0);

        //setContentView(R.layout.activity_create);

        getElementContent();

        dataView = new HashMap<String,String>();
        String url = URL_GET_USER_BY_EMAIL;
        url = url.concat(prefs.getString("email",""));
        //System.out.println(url);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        //Toast.makeText(CreateActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                        try{
                            user = new User();
                            user.setJsonData(response);
                        }
                        catch (Exception e){
                            Toast.makeText(CreateActivity.this, e.toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub

                    }
                });
        AppController.getInstance().addToRequestQueue(jsObjRequest);

    }

    private void getDataFromElement(){
        switch (property) {
            case 1:
                dataView.put("type", "apartment");
                dataView.put("no_room",field_no_room.getText().toString());
                dataView.put("no_rest_room",field_no_rest_room.getText().toString());
                break;
            case 2:
                dataView.put("type", "room");
                dataView.put("rest_room", Integer.toString(checkbox_rest_room.isChecked() ? 1 : 0) );
                break;
            case 3:
                dataView.put("type", "house");
                dataView.put("no_room",field_no_room.getText().toString());
                dataView.put("no_rest_room", field_no_rest_room.getText().toString());
                break;
            default:
                dataView.put("type", "apartment");
                dataView.put("no_room",field_no_room.getText().toString());
                dataView.put("no_rest_room", field_no_rest_room.getText().toString());
                break;
        }
        dataView.put("user_id", Integer.toString(user.getId()));
        dataView.put("price",field_price.getText().toString().trim());
        dataView.put("latitude",field_latitude.getText().toString().trim());
        dataView.put("longitude", field_longitude.getText().toString().trim());
        dataView.put("location", field_location.getText().toString().trim());
        dataView.put("area", field_area.getText().toString().trim());
        dataView.put("description",field_description.getText().toString().trim());
    }

    private void getElementContent(){
        field_price = (EditText)findViewById(R.id.field_price);
        field_latitude = (EditText)findViewById(R.id.field_latitude);
        field_longitude = (EditText)findViewById(R.id.field_longitude);
        field_location = (EditText)findViewById(R.id.field_location);
        field_area = (EditText)findViewById(R.id.field_area);
        field_no_room = (EditText)findViewById(R.id.field_no_room);
        field_no_rest_room = (EditText)findViewById(R.id.field_no_rest_room);
        field_description = (EditText)findViewById(R.id.field_description);
        checkbox_rest_room = (CheckBox)findViewById(R.id.checkbox_rest_room);
        label_no_room = (TextView)findViewById(R.id.label_no_room);
        label_no_rest_room = (TextView)findViewById(R.id.label_no_rest_room);
        btn_send = (Button)findViewById(R.id.btn_send);
        radio_apartment = (RadioButton)findViewById(R.id.radio_apartment);
        radio_room = (RadioButton)findViewById(R.id.radio_room);
        radio_house = (RadioButton)findViewById(R.id.radio_house);
    }
    public void onButtonSendClicked(View view) {
        getDataFromElement();
        //var_dump(dataView);
        //postData();
        dialog = createDialog("are you sure ?",dataView.toString());
        dialog.show();
    }
    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_apartment:
                if (checked)
                    checkbox_rest_room.setVisibility(View.GONE);
                    label_no_room.setVisibility(View.VISIBLE);
                    label_no_rest_room.setVisibility(View.VISIBLE);
                    field_no_room.setVisibility(View.VISIBLE);
                    field_no_rest_room.setVisibility(View.VISIBLE);
                    property=1;
                    break;
            case R.id.radio_room:
                if (checked)
                    checkbox_rest_room.setVisibility(View.VISIBLE);
                    label_no_room.setVisibility(View.GONE);
                    label_no_rest_room.setVisibility(View.GONE);
                    field_no_room.setVisibility(View.GONE);
                    field_no_rest_room.setVisibility(View.GONE);
                    property=2;
                    break;
            case R.id.radio_house:
                if (checked)
                    checkbox_rest_room.setVisibility(View.GONE);
                    label_no_room.setVisibility(View.VISIBLE);
                    label_no_rest_room.setVisibility(View.VISIBLE);
                    field_no_room.setVisibility(View.VISIBLE);
                    field_no_rest_room.setVisibility(View.VISIBLE);
                    property=3;
                    break;
        }
    }
    private void postData(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_POST_PROPERTY,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(CreateActivity.this, response, Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(CreateActivity.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                return dataView;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private AlertDialog createDialog(String title,String content){
        builder = new AlertDialog.Builder(CreateActivity.this);
        builder.setMessage(content)
                .setTitle(title);
        // Add the buttons
        builder.setPositiveButton("Ok. Im sure", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                postData();
            }
        });
        builder.setNegativeButton("Nope.", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //var_dump(dataView);
                // User cancelled the dialog
            }
        });
        // Set other dialog properties

        // Create the AlertDialog
        return builder.create();
    }
}
