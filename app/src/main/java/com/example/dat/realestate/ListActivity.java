package com.example.dat.realestate;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.dat.realestate.app.AppController;
import com.example.dat.realestate.models.Rental;
import com.example.dat.realestate.utils.EndlessRecyclerOnScrollListener;
import com.example.dat.realestate.utils.JsonRental;
import com.example.dat.realestate.utils.RVAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ListActivity extends navMenuActivity implements JsonRental {
    private static final int PAGE_SIZE = 8;
    private static int PAGE = 1;
    RecyclerView rv;
    List<Rental> list = new ArrayList<Rental>();

    //CustomListAdapter adapter;
    RVAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_list, null, false);
        mDrawerLayout.addView(contentView, 0);
        //setContentView(R.layout.activity_list);

        List<Rental> image_details = getListData();
        rv = (RecyclerView)findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        rv.setOnScrollListener(new EndlessRecyclerOnScrollListener(llm) {
            @Override
            public void onLoadMore(int PAGE) {
                // do something...
                //PAGE++;
                requestList("apartments", "rate", PAGE_SIZE, PAGE);
            }
        });

        //listView = (ListView) findViewById(R.id.listView);
        //adapter = new CustomListAdapter(this, image_details);
        adapter = new RVAdapter(this,image_details);
        rv.setAdapter(adapter);

//        listView.setAdapter(adapter);
//
//        // Khi người dùng click vào các ListItem
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
//                Object o = listView.getItemAtPosition(position);
//                Rental rental = (Rental) o;
//                Toast.makeText(ListActivity.this, "Selected :" + " " + rental, Toast.LENGTH_LONG).show();
//            }
//        });

        this.requestList("apartments", "rate", PAGE_SIZE, PAGE);
    }
    private List<Rental> getListData() {
        return list;
    }

    private void requestList(String type,String filter,int pageSize,int page){
        PD.show();
        String url = URL_GET_LIST;
        url = url + "type="+type+"&filter="+filter+"&pageSize="+pageSize+"&page="+page;
        //System.out.println(url);
        JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        try {
                            for (int i = 0; i< response.length(); i++){
                                JSONObject responseObj = (JSONObject) response.get(i);
                                Rental obj1 = new Rental();
                                obj1.setJsonData(responseObj);
                                list.add(obj1);
                            }
                            PD.dismiss();
                            adapter.notifyDataSetChanged();

                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req);
    }
}
