package com.example.dat.realestate.models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by hiennq on 5/2/16.
 */
public class Apartment extends Rental {
    public Apartment(){
        super();
        setTableName("Apartments");
    }
    private int no_room;
    private int no_rest_room;

    @Override
    public void setDataFromJsonObject () {
        super.setDataFromJsonObject();
        try {
            this.no_room = jsonData.getInt("no_room");
            this.no_rest_room = jsonData.getInt("no_rest_room");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setHashData(){
        super.setHashData();
        hashData.put("no_room", Integer.toString(this.no_room));
        hashData.put("no_rest_room", Integer.toString(this.no_rest_room));
    }

    public void setNo_room(int no_room){this.no_room = no_room;}
    public int getNo_room(int no_room){return this.no_room;}
    public void setNo_rest_room(int no_rest_room){this.no_rest_room = no_rest_room;}
    public int getNo_rest_room(int no_rest_room){return this.no_rest_room;}
}
