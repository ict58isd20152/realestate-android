# README #

TODO: Before you run your application, you need a Google Maps API key.

To get one, follow this link, follow the directions and press "Create" at the end:

```
    https://console.developers.google.com/flows/enableapi?apiid=maps_android_backend&keyType=CLIENT_SIDE_ANDROID&r=60:45:50:6C:E3:D5:95:3D:B6:4B:87:14:5D:55:49:CC:39:CA:2E:79%3Bcom.example.dat.realestate
```

You can also add your credentials to an existing key, using this line:
    
```
60:45:50:6C:E3:D5:95:3D:B6:4B:87:14:5D:55:49:CC:39:CA:2E:79;com.example.dat.realestate
```

Alternatively, follow the directions here:
```
    https://developers.google.com/maps/documentation/android/start#get-key
```

Once you have your key (it starts with "AIza"), replace the "google_maps_key"
    string in this file. `google_maps_api.xml`